<?php

declare(strict_types=1);

namespace Paycoiner\Client\Validators;

use Paycoiner\Client\Models\Model;

abstract class Validator
{
    /** @var string */
    protected $key;

    public function __construct(string $key)
    {
        $this->key = $key;
    }

    /**
     * @param array|Model $data
     */
    abstract public function isValid($data): bool;
}
