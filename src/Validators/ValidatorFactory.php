<?php

declare(strict_types=1);

namespace Paycoiner\Client\Validators;

use Paycoiner\Client\Exceptions\EnvironmentException;
use Paycoiner\Client\Exceptions\Jwt\InvalidKey;
use Paycoiner\Client\Exceptions\PaycoinerClientException;
use Paycoiner\Client\Models\Environment;

class ValidatorFactory
{
    /** @var Environment */
    private $environment;

    public function __construct(Environment $environment = null)
    {
        if ($environment === null) {
            $environment = Environment::loadFromEnv();
        }
        $this->environment = $environment;
    }

    /**
     * @return AddressSignatureValidator
     * @throws InvalidKey
     * @throws PaycoinerClientException
     * @throws EnvironmentException
     */
    public function getAddressSignatureValidator(): AddressSignatureValidator
    {
        $publicKey = $this->environment->getServicePublicKey();
        if (! $publicKey) {
            throw new EnvironmentException(
                'The Environment should have paycoiner public key for ' .
                AddressSignatureValidator::class .
                ' initialization.'
            );
        }

        return new AddressSignatureValidator($publicKey);
    }
}
