<?php

declare(strict_types=1);

namespace Paycoiner\Client\Services;

use Paycoiner\Client\Enums\HttpMethod;

class HmacService
{
    public function getHmac(string $payload, string $key): string
    {
        return hash_hmac('sha256', $payload, $key);
    }

    public function validateHmac(string $hash, string $key, array $payload): bool
    {
        return hash_equals($this->getHmac(json_encode($payload), $key), $hash);
    }

    public function validateHmacPayload(string $hash, string $key, string $payload): bool
    {
        return hash_equals($this->getHmac($payload, $key), $hash);
    }

    public function getRawData(array $data, string $url, HttpMethod $method): string
    {
        if ($method->isEquals(HttpMethod::GET())) {
            return '/' . $url . (empty($data) ? '' : '?' . http_build_query($data));
        }

        return (string)json_encode($data);
    }

    /**
     * @param array $data
     * @param array $headers
     * @param string $url
     * @param HttpMethod $method
     * @param string $endpointKey
     *
     * @return void
     */
    public function appendHashToRequest(&$data, &$headers, $url, $method, $endpointKey)
    {
        if (isset($data['customer_id'])) {
            $headers['X-Customer-Id'] = $data['customer_id'];
        }

        $content = $this->getHmac($this->getRawData($data, $url, $method), $endpointKey);
        $headers['X-Api-Key'] = $content;
        $headers['HMAC'] = $content; // backward compatibility
    }
}
