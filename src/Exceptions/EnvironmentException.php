<?php

declare(strict_types=1);

namespace Paycoiner\Client\Exceptions;

use Throwable;

class EnvironmentException extends PaycoinerClientException
{
    public function __construct(string $data = '', Throwable $previous = null)
    {
        parent::__construct('ENV', $data, $previous);
    }
}
