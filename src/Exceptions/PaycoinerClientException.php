<?php

declare(strict_types=1);

namespace Paycoiner\Client\Exceptions;

use Exception;
use Throwable;

class PaycoinerClientException extends Exception
{
    /** @var string */
    public $flag;
    /** @var string */
    public $data;

    public function __construct(string $flag = '', string $data = '', Throwable $previous = null)
    {
        parent::__construct($data, 0, $previous);

        $this->flag = $flag;
        $this->data = $data;
    }
}
