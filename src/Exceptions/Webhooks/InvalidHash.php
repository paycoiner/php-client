<?php

declare(strict_types=1);

namespace Paycoiner\Client\Exceptions\Webhooks;

use Paycoiner\Client\Exceptions\PaycoinerClientException;

class InvalidHash extends PaycoinerClientException
{
}
