<?php

declare(strict_types=1);

namespace Paycoiner\Client\Handlers;

use Paycoiner\Client\Exceptions\Webhooks\InvalidHash;
use Paycoiner\Client\Services\HmacService;

abstract class HmacHandler extends Handler
{
    /** @var HmacService */
    protected $hmacService;

    public function __construct(string $webhookKey)
    {
        parent::__construct($webhookKey);
        $this->hmacService = new HmacService();
    }

    protected function check(array $data)
    {
        if (false === isset($_SERVER['HTTP_HMAC'])) {
            throw new InvalidHash();
        }
        $hmac = $_SERVER['HTTP_HMAC'];
        if (false === $this->hmacService->validateHmac($hmac, $this->webhookKey, $data)) {
            throw new InvalidHash();
        }
    }
}
