<?php

declare(strict_types=1);

namespace Paycoiner\Client\Handlers;

use Paycoiner\Client\Exceptions\EnvironmentException;
use Paycoiner\Client\Models\Environment;

class HandlerFactory
{
    /** @var Environment */
    private $environment;

    /** @var DepositReceivedHandler */
    private $depositReceivedHandler;

    /** @var PaymentHandler */
    private $paymentHandler;

    /** @var PayoutStatusHandler */
    private $payoutStatusHandler;

    public function __construct(Environment $environment = null)
    {
        if ($environment === null) {
            $environment = Environment::loadFromEnv();
        }
        $this->environment = $environment;
    }

    /** @throws EnvironmentException */
    private function handlerInitializer(string $handlerClass, Handler &$handler = null): Handler
    {
        if ($handler !== null) {
            return $handler;
        }
        if (! $this->environment->getWebhookKey()) {
            throw new EnvironmentException(
                'The Environment should have webhookKey for ' . $handlerClass . ' initialization.'
            );
        }
        $handler = new $handlerClass($this->environment->getWebhookKey());

        return $handler;
    }

    /** @throws EnvironmentException */
    public function getDepositReceivedHandler(): DepositReceivedHandler
    {
        $this->handlerInitializer(DepositReceivedHandler::class, $this->depositReceivedHandler);

        return $this->depositReceivedHandler;
    }

    /** @throws EnvironmentException */
    public function getPaymentHandler(): PaymentHandler
    {
        $this->handlerInitializer(PaymentHandler::class, $this->paymentHandler);

        return $this->paymentHandler;
    }

    /** @throws EnvironmentException */
    public function getPayoutStatusHandler(): PayoutStatusHandler
    {
        $this->handlerInitializer(PayoutStatusHandler::class, $this->payoutStatusHandler);

        return $this->payoutStatusHandler;
    }
}
