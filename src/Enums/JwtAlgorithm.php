<?php

declare(strict_types=1);

namespace Paycoiner\Client\Enums;

/**
 * @method static static RS256()
 */
class JwtAlgorithm extends Enum
{
    public const RS256 = 'RS256';
}
