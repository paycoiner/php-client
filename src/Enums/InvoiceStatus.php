<?php

declare(strict_types=1);

namespace Paycoiner\Client\Enums;

/**
 * @method static self NEW()
 * @method static self PENDING()
 * @method static self OVERPAID()
 * @method static self UNDERPAID()
 * @method static self SUCCESSFUL()
 *
 * @method static self EXPIRED()
 * @method static self UNSUCCESSFUL()
 * @method static self UNKNOWN()
 */
final class InvoiceStatus extends Enum
{
    /**
     * pending time above 24h, paid but 0 confirmations
     */
    public const UNSUCCESSFUL = 'UNSUCCESSFUL';

    /**
     * transaction count (no pending transactions while … ) = 0 and time for payment expired
     */
    public const EXPIRED = 'EXPIRED';

    /**
     * newly generated payment request (invoice), user hasn't made any transfer yet and there is still time for payment)
     */
    public const NEW = 'NEW';

    /**
     * transaction count > 0 and not all transactions are completed
     */
    public const PENDING = 'PENDING';

    /**
     * amount received > amount stated, based on confirmed transactions
     */
    public const OVERPAID = 'OVERPAID';

    /**
     * amount received < amount stated, based on confirmed transactions
     */
    public const UNDERPAID = 'UNDERPAID';

    /**
     * amount received = amount stated, based on confirmed transactions
     */
    public const SUCCESSFUL = 'SUCCESSFUL';

    /**
     * situation when was added one more status in payment service, but not added in client.
     */
    public const UNKNOWN = 'UNKNOWN';
}
