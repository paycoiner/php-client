<?php

declare(strict_types=1);

namespace Paycoiner\Client\Models\Requests;

use Paycoiner\Client\Models\Model;

class GetNextAddressRequest extends Model
{
    /**
     * uuid
     * @var string
     */
    public $customerId;

    /**
     * uuid from customer system
     * @var string
     */
    public $orderId;

    /**
     * uppercase, example: BTC
     * @var string
     */
    public $currency;

    public function __construct(
        string $customerId,
        string $orderId,
        string $currency
    ) {
        $this->customerId = $customerId;
        $this->orderId = $orderId;
        $this->currency = strtoupper($currency);
    }
}
