<?php

declare(strict_types=1);

namespace Paycoiner\Client\Models\Requests;

use Paycoiner\Client\Models\Model;

class PayoutRequest extends Model
{
    /**
     * to which you want to send {amount}
     * @var string
     */
    public $address;

    /**
     * uuid
     * @var string
     */
    public $customerId;

    /**
     * uuid from customer system
     * @var string
     */
    public $orderId;

    /**
     * uppercase, example: BTC
     * @var string
     */
    public $currency;

    /**
     * the exact amount to be sent to {address}
     * @var string
     */
    public $amount;

    public function __construct(
        string $customerId,
        string $orderId,
        string $address,
        string $currency,
        string $amount
    ) {
        $this->customerId = $customerId;
        $this->orderId = $orderId;
        $this->address = $address;
        $this->currency = strtoupper($currency);
        $this->amount = $amount;
    }
}
