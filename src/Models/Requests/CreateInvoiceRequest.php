<?php

declare(strict_types=1);

namespace Paycoiner\Client\Models\Requests;

use Paycoiner\Client\Models\Model;

class CreateInvoiceRequest extends Model
{
    /**
     * Your customer ID (UUID)
     * @var string
     */
    public $customerId;

    /**
     * Payment identifier. It must be unique. We reject requests with replicated orderId.
     * @var string
     */
    public $orderId;

    /**
     * Currency ticker. uppercase, example: BTC
     * @var string
     */
    public $baseCurrency;

    /**
     * Cryptocurrency ticker, user will transfer money in this currency. example: BTC
     * @var string
     */
    public $quoteCurrency;

    /**
     * The total amount of the payment (in baseCurrency).
     * @var string
     */
    public $amount;

    /**
     * Buyer's e-mail.
     * @var string|null
     */
    public $email;

    public function __construct(
        string $customerId,
        string $orderId,
        string $baseCurrency,
        string $quoteCurrency,
        string $amount,
        string $email = null
    ) {
        $this->customerId = $customerId;
        $this->orderId = $orderId;
        $this->baseCurrency = strtoupper($baseCurrency);
        $this->quoteCurrency = strtoupper($quoteCurrency);
        $this->amount = $amount;
        $this->email = $email;
    }
}
