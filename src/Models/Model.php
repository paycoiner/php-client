<?php

declare(strict_types=1);

namespace Paycoiner\Client\Models;

use JsonSerializable;
use Paycoiner\Client\Exceptions\ValidationException;
use ReflectionClass;
use TypeError;

abstract class Model implements JsonSerializable
{
    /** @throws ValidationException */
    final protected static function getConstructorArgsFromArray(array $data): array
    {
        $reflection = new ReflectionClass(static::class);
        // define constructor
        $constructor = $reflection->getConstructor();
        if ($constructor === null) { // when class don't have constructor than nothing to do.
            return [];
        }

        $parameters = $constructor->getParameters();
        $args = [];
        foreach ($parameters as $parameter) {
            if (isset($data[$parameter->name])) {
                $args[$parameter->name] = $data[$parameter->name];
                continue;
            }

            if ($parameter->isDefaultValueAvailable()) {
                /** @noinspection PhpUnhandledExceptionInspection */
                $args[$parameter->name] = $parameter->getDefaultValue();
            } elseif ($parameter->allowsNull()) {
                $args[$parameter->name] = null;
            } else {
                throw new ValidationException("{$parameter->name} is not defined in data array");
            }
        }

        return $args;
    }

    /**
     * @return static
     *
     * @throws ValidationException
     */
    public static function fromArray(array $data)
    {
        $definedData = static::getConstructorArgsFromArray($data);

        // create new instance by constructor args
        $reflection = new ReflectionClass(static::class);
        /** @var static $instance */
        $instance = $reflection->newInstanceArgs($definedData);

        // all properties defined by constructor should be omitted because they are already validated.
        $omitProperties = array_keys($definedData);
        // set properties by setters
        $instance->setPropertiesFromArray($data, $omitProperties);
        // validate that all required properties was successfully sets
        $instance->validateModel($omitProperties);

        return $instance;
    }

    /** @throws ValidationException */
    final protected function setPropertiesFromArray(array $data, array $omitProperties = [])
    {
        foreach ($data as $property => $value) {
            if ($value === null || in_array($property, $omitProperties, true)) {
                continue;
            }
            $setterMethod = 'set' . ucfirst($property);
            if (method_exists($this, $setterMethod)) {
                try {
                    $this->{$setterMethod}($value);
                } catch (TypeError $typeError) {
                    throw new ValidationException(
                        static::class . " is invalid because the property \"{$property}\" has invalid type.",
                        $typeError
                    );
                }
                continue;
            }
            if (property_exists($this, $property)) {
                $this->{$property} = $value;
            }
        }
    }

    /** @throws ValidationException */
    final protected function validateModel(array $omitProperties = [])
    {
        $data = $this->toArray();
        $omitProperties = array_merge($omitProperties, $this->getNotRequiredProperties());
        foreach ($data as $property => $value) {
            if (in_array($property, $omitProperties, true)) {
                continue;
            }
            $getterMethod = 'get' . ucfirst($property);
            if (method_exists($this, $getterMethod)) {
                try {
                    $value = $this->{$getterMethod}();
                } catch (TypeError $typeError) {
                    throw new ValidationException(
                        static::class . " is invalid because the property \"{$property}\" has invalid type.",
                        $typeError
                    );
                }
            }
            if ($value === null) {
                throw new ValidationException(
                    static::class . " is invalid because the property \"{$property}\" has not specified."
                );
            }
        }
    }

    protected function getNotRequiredProperties(): array
    {
        return [];
    }

    public function __toString(): string
    {
        return (string) json_encode($this->toArray());
    }

    public function toArray(): array
    {
        return get_object_vars($this);
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }
}
