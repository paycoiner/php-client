<?php

declare(strict_types=1);

namespace Paycoiner\Client\Models\Responses;

use Paycoiner\Client\Models\Model;

class Ticket extends Model
{
    /** @var string */
    private $uuid; // example: "2df241e0-ff88-11e8-a5e5-507b9dfbcb8f"
    /** @var string */
    private $base; // example: "USD"
    /** @var string */
    private $quote; // example: "BTC"
    /** @var string */
    private $rate; // example: "0.000176047349695174015130312936"
    /** @var int */
    private $timestamp; // example: 1542236399

    public function __construct(string $uuid, string $base, string $quote, string $rate, int $timestamp)
    {
        $this->uuid = $uuid;
        $this->base = $base;
        $this->quote = $quote;
        $this->rate = $rate;
        $this->timestamp = $timestamp;
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function getBase(): string
    {
        return $this->base;
    }

    public function getQuote(): string
    {
        return $this->quote;
    }

    public function getRate(): string
    {
        return $this->rate;
    }

    public function getTimestamp(): int
    {
        return $this->timestamp;
    }
}
