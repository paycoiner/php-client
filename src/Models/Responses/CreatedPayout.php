<?php

declare(strict_types=1);

namespace Paycoiner\Client\Models\Responses;

use Paycoiner\Client\Models\Model;
use Paycoiner\Client\Models\Webhooks\PayoutStatus;

class CreatedPayout extends Model
{
    /**
     * uuid, created in PayCoiner API.
     * Shipped in @see PayoutStatus so that the customer can determine exactly
     * which payment was returned in the callback
     *
     * @var string
     */
    protected $payoutId;

    /**
     * uuid
     * @var string
     */
    protected $customerId;

    /**
     * uuid from customer system
     * @var string
     */
    protected $orderId;

    public function __construct(string $payoutId, string $customerId, string $orderId)
    {
        $this->payoutId = $payoutId;
        $this->customerId = $customerId;
        $this->orderId = $orderId;
    }

    public function getPayoutId(): string
    {
        return $this->payoutId;
    }

    public function getCustomerId(): string
    {
        return $this->customerId;
    }

    public function getOrderId(): string
    {
        return $this->orderId;
    }
}
