<?php

declare(strict_types=1);

namespace Paycoiner\Client\Models\Responses;

use Paycoiner\Client\Enums\HttpStatus;
use Paycoiner\Client\Models\Model;

class Response extends Model
{
    /** @var HttpStatus */
    protected $status;
    /** @var mixed */
    protected $response;

    public function __construct(HttpStatus $status, $response)
    {
        $this->status = $status;
        $this->response = $response;
    }

    public function getStatus(): HttpStatus
    {
        return $this->status;
    }

    public function getResponse()
    {
        return $this->response;
    }
}
