<?php

declare(strict_types=1);

namespace Paycoiner\Client\Clients;

use GuzzleHttp\RequestOptions;
use Paycoiner\Client\Enums\HttpMethod;
use Paycoiner\Client\Exceptions\Jwt\InvalidKey;
use Paycoiner\Client\Services\HmacService;
use Paycoiner\Client\Services\JwtService;

abstract class JwtClient extends Client
{
    /** @var JwtService */
    protected $jwtService;
    /** @var HmacService */
    protected $hmacService;

    /**
     * @throws InvalidKey
     */
    public function __construct(string $baseUri, string $privateKeyOrPath, int $clientTimeOut = null)
    {
        parent::__construct($baseUri, '', $clientTimeOut);

        $this->jwtService = new JwtService();
        $this->hmacService = new HmacService();
        $this->endpointKey = $this->jwtService->getPrivateKey($privateKeyOrPath);
    }

    /**
     * @param array|string $data
     * @param array $headers
     * @param string $url
     * @param HttpMethod $method
     *
     * @return void
     */
    protected function appendHashToRequest(&$data, &$headers, $url, $method)
    {
        if ($method->isEquals(HttpMethod::GET())) {
            $this->hmacService->appendHashToRequest($data, $headers, $url, $method, $this->endpointKey);
            return;
        }

        $data = $this->jwtService->encode($data, $this->endpointKey);
    }

    public function getRequestBodyType(HttpMethod $httpMethod): string
    {
        return $httpMethod->isEquals(HttpMethod::GET()) ? RequestOptions::QUERY : RequestOptions::BODY;
    }
}
