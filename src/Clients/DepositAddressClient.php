<?php

declare(strict_types=1);

namespace Paycoiner\Client\Clients;

use GuzzleHttp\Exception\GuzzleException;
use Paycoiner\Client\Enums\HttpMethod;
use Paycoiner\Client\Exceptions\ValidationException;
use Paycoiner\Client\Models\Requests\GetNextAddressRequest;
use Paycoiner\Client\Models\Responses\DepositAddress;

class DepositAddressClient extends HmacClient
{
    public const API_VERSION_PREFIX = 'api/v1';

    /**
     * @throws GuzzleException
     * @throws ValidationException
     * @throws \Paycoiner\Client\Exceptions\Endpoints\UnauthorizedRequest
     * @throws \Paycoiner\Client\Exceptions\Endpoints\UnprocessableRequest
     * @throws \Paycoiner\Client\Exceptions\PaycoinerClientException
     */
    public function getNext(GetNextAddressRequest $request): DepositAddress
    {
        $result = $this->send(HttpMethod::POST(), self::API_VERSION_PREFIX . '/address', $request->toArray());

        return DepositAddress::fromArray($result->getResponse());
    }

    /**
     * @throws GuzzleException
     * @throws ValidationException
     * @throws \Paycoiner\Client\Exceptions\Endpoints\UnprocessableRequest
     * @throws \Paycoiner\Client\Exceptions\Endpoints\NotFound
     * @throws \Paycoiner\Client\Exceptions\PaycoinerClientException
     */
    public function getInfo(string $uuid): DepositAddress
    {
        $result = $this->send(HttpMethod::GET(), self::API_VERSION_PREFIX . '/address/' . $uuid);

        return DepositAddress::fromArray($result->getResponse());
    }
}
