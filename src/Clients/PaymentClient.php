<?php

declare(strict_types=1);

namespace Paycoiner\Client\Clients;

use Paycoiner\Client\Enums\HttpMethod;
use Paycoiner\Client\Models\Requests\CreateInvoiceRequest;
use Paycoiner\Client\Models\Responses\Invoice;

class PaymentClient extends HmacClient
{
    public const API_VERSION_PREFIX = 'api/v1';

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Paycoiner\Client\Exceptions\Endpoints\UnauthorizedRequest
     * @throws \Paycoiner\Client\Exceptions\Endpoints\UnprocessableRequest
     * @throws \Paycoiner\Client\Exceptions\PaycoinerClientException
     * @throws \Paycoiner\Client\Exceptions\ValidationException
     */
    public function createInvoice(CreateInvoiceRequest $request): Invoice
    {
        $response = $this->send(HttpMethod::POST(), self::API_VERSION_PREFIX . '/invoices', $request->toArray());

        return Invoice::fromArray($response->getResponse());
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Paycoiner\Client\Exceptions\Endpoints\NotFound
     * @throws \Paycoiner\Client\Exceptions\Endpoints\UnprocessableRequest
     * @throws \Paycoiner\Client\Exceptions\PaycoinerClientException
     * @throws \Paycoiner\Client\Exceptions\ValidationException
     */
    public function getInvoice(string $invoiceId): Invoice
    {
        $response = $this->send(HttpMethod::GET(), self::API_VERSION_PREFIX . '/invoices/' . $invoiceId);

        return Invoice::fromArray($response->getResponse());
    }

    /**
     * @return array
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Paycoiner\Client\Exceptions\Endpoints\NotFound
     * @throws \Paycoiner\Client\Exceptions\Endpoints\UnprocessableRequest
     * @throws \Paycoiner\Client\Exceptions\PaycoinerClientException
     * @throws \Paycoiner\Client\Exceptions\ValidationException
     */
    public function getBalance(string $customerId)
    {
        $response = $this->send(
            HttpMethod::GET(),
            self::API_VERSION_PREFIX . '/balance',
            ['customer_id' => $customerId, 'nonce' => time()]
        );

        return $response->getResponse();
    }
}
