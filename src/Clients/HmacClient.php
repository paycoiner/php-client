<?php

declare(strict_types=1);

namespace Paycoiner\Client\Clients;

use Paycoiner\Client\Services\HmacService;

abstract class HmacClient extends Client
{
    /** @var HmacService */
    protected $hmacService;

    public function __construct(string $baseUri, string $endpointKey, int $clientTimeOut = null)
    {
        parent::__construct($baseUri, $endpointKey, $clientTimeOut);

        $this->hmacService = new HmacService();
    }

    protected function appendHashToRequest(&$data, &$headers, $url, $method)
    {
        $this->hmacService->appendHashToRequest($data, $headers, $url, $method, $this->endpointKey);
    }
}
