<?php

declare(strict_types=1);

namespace Tests\Unit\Payments;

use Paycoiner\Client\Clients\PaymentClient;
use Paycoiner\Client\Enums\HttpStatus;
use Paycoiner\Client\Enums\InvoiceStatus;
use Paycoiner\Client\Exceptions\Endpoints\NotFound;
use Paycoiner\Client\Exceptions\Endpoints\UnauthorizedRequest;
use Paycoiner\Client\Models\Requests\CreateInvoiceRequest;
use Paycoiner\Client\Models\Responses\Invoice;
use Tests\TestCase;

class ClientTest extends TestCase
{
    private function getMockedClient(HttpStatus $status = null, string $mockPath = null): PaymentClient
    {
        $client = new PaymentClient($this->faker->url, '');
        $client->setClient($this->getMockedGuzzleClient($status, $mockPath));

        return $client;
    }

    /**
     * @group client
     * @group payment
     */
    public function testUnauthorizedRequest()
    {
        $this->expectException(UnauthorizedRequest::class);

        $clientMock = $this->getMockedClient(HttpStatus::UNAUTHORIZED());
        $clientMock->createInvoice(new CreateInvoiceRequest('', '', '', '', ''));
    }

    /**
     * @group client
     * @group payment
     */
    public function testNotFound()
    {
        $this->expectException(NotFound::class);

        $clientMock = $this->getMockedClient(HttpStatus::NOT_FOUND());
        $clientMock->getInvoice('');
    }

    /**
     * @group client
     * @group payment
     */
    public function testSuccessfullyCreating()
    {
        $clientMock = $this->getMockedClient(HttpStatus::OK(), 'payments/invoice.json');
        $request = new CreateInvoiceRequest(
            'e9d43ed2-f253-11e8-9179-fa163ee532b5',
            '1547473364088',
            'BTC',
            'BTC',
            '0.00002'
        );

        $this->assertInvoiceResponse($clientMock->createInvoice($request));
    }

    /**
     * @group client
     * @group payment
     */
    public function testSuccessfullyGetInvoice()
    {
        $clientMock = $this->getMockedClient(HttpStatus::OK(), 'payments/invoice.json');
        $this->assertInvoiceResponse($clientMock->getInvoice('45511162-1802-11e9-ae97-fa163e025a6a'));
    }

    /**
     * @group client
     * @group payment
     */
    public function testBalance()
    {
        $clientMock = $this->getMockedClient(HttpStatus::OK(), 'payments/balance.json');
        self::assertSame(
            [
                'LTC' => '1.01',
                'BTC' => '0.01',
                'ETH' => '2.35',
            ],
            $clientMock->getBalance('e9d43ed2-f253-11e8-9179-fa163ee532b5')
        );
    }

    protected function assertInvoiceResponse(Invoice $response)
    {
        $this->assertSame('1547473364088', $response->getOrderId());
        $this->assertSame('BTC', $response->getCurrency());
        $this->assertSame('2N9xESQREzgsbSyY78C2RoshV1qA4L3kY2o', $response->getAddress());
        $this->assertSame(
            'o34CoumNxCmnYMVd8qu2LQfWLdY3IwXrnUzYa01yu1l+tnDrnpH1S/svFy9p/abkMksLpgyC9KvGdAIvw/gFQ3K1Z8/NRbKZWqJN2Tt4AuZfeDI0n3B/VCNkmLDIPhu0lPnlJEsb9KAvPNlCPcYuLdO0VKUag6cVz41lv7AQAaM=',
            $response->getSignature()
        );
        $this->assertSame('45511162-1802-11e9-ae97-fa163e025a6a', $response->getInvoiceId());
        $this->assertSame('https://demo.paycoiner.com/invoice/view/?id=45511162-1802-11e9-ae97-fa163e025a6a', $response->getUrl());
        $this->assertSame('2019-01-14T13:57:44+00:00', $response->getExpireAt()->toIso8601String());
        $this->assertSame('2019-01-14T13:42:44+00:00', $response->getCreatedAt()->toIso8601String());
        $this->assertSame(InvoiceStatus::NEW, $response->getStatus()->getValue());
        $this->assertSame('0.00002', $response->getBaseAmount());
        $this->assertSame('BTC', $response->getBaseCurrency());
        $this->assertSame('0.00002', $response->getAmount());
        $this->assertSame('0', $response->getPaidConfirmed());
        $this->assertSame('0', $response->getPaidUnconfirmed());
        $this->assertSame('0', $response->getPaid());
        $this->assertSame('0.00002', $response->getMissing());
        $this->assertSame('1', $response->getCurrencyRate());
    }
}
