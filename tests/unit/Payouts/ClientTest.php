<?php

declare(strict_types=1);

namespace Tests\Unit\Payouts;

use Paycoiner\Client\Clients\PayoutClient;
use Paycoiner\Client\Enums\HttpStatus;
use Paycoiner\Client\Exceptions\Endpoints\UnauthorizedRequest;
use Paycoiner\Client\Exceptions\Endpoints\UnprocessableRequest;
use Paycoiner\Client\Exceptions\ValidationException;
use Paycoiner\Client\Models\Requests\PayoutRequest;
use Tests\TestCase;

class ClientTest extends TestCase
{
    private function getMockedClient(HttpStatus $status = null, string $mockPath = null): PayoutClient
    {
        $client = new PayoutClient($this->faker->url, __DIR__ . '/../../mocks/keys/private_key.pem');
        $client->setClient($this->getMockedGuzzleClient($status, $mockPath));

        return $client;
    }

    private function getEmptyPayoutRequest(): PayoutRequest
    {
        return new PayoutRequest('', '', '', '', '');
    }

    /**
     * @group client
     * @group payout
     */
    public function testIncorrectPayoutResponse()
    {
        $this->expectException(ValidationException::class);

        $clientMock = $this->getMockedClient();
        $clientMock->create($this->getEmptyPayoutRequest());
    }

    /**
     * @group client
     * @group payout
     */
    public function testBadPayoutResponse()
    {
        $this->expectException(UnprocessableRequest::class);

        $clientMock = $this->getMockedClient(HttpStatus::UNPROCESSABLE_ENTITY());
        $clientMock->create($this->getEmptyPayoutRequest());
    }

    /**
     * @group client
     * @group payout
     */
    public function testUnauthorizedRequest()
    {
        $this->expectException(UnauthorizedRequest::class);

        $clientMock = $this->getMockedClient(HttpStatus::UNAUTHORIZED());
        $clientMock->create($this->getEmptyPayoutRequest());
    }

    /**
     * @group client
     * @group payout
     */
    public function testSuccessfullyCreatedPayoutResponse()
    {
        $clientMock = $this->getMockedClient(HttpStatus::OK(), 'payouts/successfully-created.json');
        /** @var PayoutRequest $payoutRequest */
        $payoutRequest = PayoutRequest::fromArray(
            json_decode($this->getMockData('payouts/successfully-broadcasted.json'), true)
        );
        /** @var \Paycoiner\Client\Models\Responses\CreatedPayout $payoutResponse */
        $payoutResponse = $clientMock->create($payoutRequest);

        $this->assertEquals($payoutResponse->getCustomerId(), $payoutRequest->customerId);
        $this->assertEquals($payoutResponse->getOrderId(), $payoutRequest->orderId);
        $this->assertNotNull($payoutResponse->getPayoutId());
    }

    /**
     * @group client
     * @group payment
     */
    public function testBalance()
    {
        $clientMock = $this->getMockedClient(HttpStatus::OK(), 'payouts/balance.json');
        self::assertSame(
            [
                'LTC' => '1.01',
                'BTC' => '0.01',
                'ETH' => '2.35',
            ],
            $clientMock->getBalance('e9d43ed2-f253-11e8-9179-fa163ee532b5')
        );
    }
}
