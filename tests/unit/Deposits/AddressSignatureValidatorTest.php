<?php

declare(strict_types=1);

namespace Tests\Unit\Deposits;

use Paycoiner\Client\Validators\AddressSignatureValidator;
use Tests\TestCase;

class AddressSignatureValidatorTest extends TestCase
{
    private function getInlineData(array $data): string
    {
        return $data['address'] . $data['currency'] . $data['paymentTag'];
    }

    private function getPrivateKeyPath(): string
    {
        return __DIR__ . '/../../mocks/keys/private_key.pem';
    }

    private function getPublicKeyPath(): string
    {
        return __DIR__ . '/../../mocks/keys/public_key.pem';
    }

    private function generateSignature(array $data): string
    {
        openssl_sign(
            $this->getInlineData($data),
            $signature,
            file_get_contents($this->getPrivateKeyPath()),
            OPENSSL_ALGO_SHA256
        );

        return base64_encode($signature);
    }

    public function testSuccessValidation()
    {
        $data = [
            'address' => md5((string) time()),
            'currency' => 'BTC',
            'paymentTag' => '',
        ];
        $data['signature'] = $this->generateSignature($data);

        $validator = new AddressSignatureValidator($this->getPublicKeyPath());

        // ok data
        $this->assertTrue($validator->isValid($data));

        // invalid data
        $data['currency'] = 'ETH';
        $this->assertFalse($validator->isValid($data));

        // not all data set
        unset($data['currency']);
        $this->assertFalse($validator->isValid($data));
    }
}
