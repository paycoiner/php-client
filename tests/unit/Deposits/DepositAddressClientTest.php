<?php

declare(strict_types=1);

namespace Tests\Unit\Deposits;

use Paycoiner\Client\Clients\DepositAddressClient;
use Paycoiner\Client\Enums\HttpStatus;
use Paycoiner\Client\Exceptions\Endpoints\NotFound;
use Paycoiner\Client\Exceptions\Endpoints\UnauthorizedRequest;
use Paycoiner\Client\Models\Requests\GetNextAddressRequest;
use Paycoiner\Client\Models\Responses\DepositAddress;
use Tests\TestCase;

class DepositAddressClientTest extends TestCase
{
    private function getMockedClient(HttpStatus $status = null, string $mockPath = null): DepositAddressClient
    {
        $client = new DepositAddressClient($this->faker->url, '');
        $client->setClient($this->getMockedGuzzleClient($status, $mockPath));

        return $client;
    }

    /**
     * @group client
     * @group deposit-address
     */
    public function testUnauthorizedRequest()
    {
        $this->expectException(UnauthorizedRequest::class);

        $clientMock = $this->getMockedClient(HttpStatus::UNAUTHORIZED());
        $clientMock->getNext(new GetNextAddressRequest('', '', ''));
    }


    /**
     * @group client
     * @group deposit-address
     */
    public function testNotFound()
    {
        $this->expectException(NotFound::class);

        $clientMock = $this->getMockedClient(HttpStatus::NOT_FOUND());
        $clientMock->getInfo('');
    }

    /**
     * @group client
     * @group deposit-address
     */
    public function testSuccessfullyGotNewAddress()
    {
        $clientMock = $this->getMockedClient(HttpStatus::OK(), 'deposits/address.json');
        $addressRequest = new GetNextAddressRequest(
            'e9d43ed2-f253-11e8-9179-fa163ee532b5',
            '1547473364088',
            'BTC'
        );

        $this->assertAddressResponse($clientMock->getNext($addressRequest));
    }

    /**
     * @group client
     * @group deposit-address
     */
    public function testSuccessfullyGetAddressInfo()
    {
        $clientMock = $this->getMockedClient(HttpStatus::OK(), 'deposits/address.json');
        $this->assertAddressResponse($clientMock->getInfo('d17e5e28-23b1-11e9-a439-507b9dfbcb8f'));
    }

    protected function assertAddressResponse(DepositAddress $response)
    {

        $this->assertEquals('1547473364088', $response->getOrderId());
        $this->assertEquals('BTC', $response->getCurrency());
        $this->assertEquals('d17e5e28-23b1-11e9-a439-507b9dfbcb8f', $response->getId());
        $this->assertEquals('rlEVA1TFr3QeRFoNlYM7LufsRrA8T', $response->getAddress());
        $this->assertEquals(
            'o34CoumNxCmnYMVd8qu2LQfWLdY3IwXrnUzYa01yu1l+tnDrnpH1S/svFy9p/abkMksLpgyC9KvGdAIvw/gFQ3K1Z8/NRbKZWqJN2Tt4AuZfeDI0n3B/VCNkmLDIPhu0lPnlJEsb9KAvPNlCPcYuLdO0VKUag6cVz41lv7AQAaM=',
            $response->getSignature()
        );
    }
}
