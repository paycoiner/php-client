<?php

declare(strict_types=1);

namespace Tests\Unit;

use Paycoiner\Client\Clients\AddressValidatorClient;
use Paycoiner\Client\Enums\HttpStatus;
use Paycoiner\Client\Models\Requests\AddressValidateRequest;
use Tests\TestCase;

class AddressValidatorClientTest extends TestCase
{
    private function getMockedClient(HttpStatus $status = null, string $mockPath = null): AddressValidatorClient
    {
        $client = new AddressValidatorClient($this->faker->url);
        $client->setClient($this->getMockedGuzzleClient($status, $mockPath));

        return $client;
    }

    private function getRequest(): AddressValidateRequest
    {
        return new AddressValidateRequest(md5((string) time()), 'BTC');
    }

    public function testValid()
    {
        $this->assertTrue(
            $this->getMockedClient(
                HttpStatus::OK(),
                'address-validator/valid.json'
            )->validate($this->getRequest())
        );
    }

    public function testFailed()
    {
        $this->assertNull(
            $this->getMockedClient(
                HttpStatus::OK(),
                'address-validator/failed.json'
            )->validate($this->getRequest())
        );
    }
}
