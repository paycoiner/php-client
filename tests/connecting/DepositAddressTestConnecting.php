<?php

declare(strict_types=1);

namespace Tests\Connecting;

use Paycoiner\Client\Clients\DepositAddressClient;
use Paycoiner\Client\Exceptions\Endpoints\UnauthorizedRequest;
use Paycoiner\Client\Models\Requests\GetNextAddressRequest;
use Tests\TestCase;

class DepositAddressTestConnecting extends TestCase
{
    public function testGot()
    {
        $request = new GetNextAddressRequest(
            '00e55aac-2e7d-11e8-a650-507b9dfbcb8f',
            time() . md5((string) time()),
            'BTC'
        );
        $client = new DepositAddressClient('http://deposit-addresses.paycoiner.loc', 'eyJpdiI6Ik9nMU15clNEVWxlSHoxVWxESStGM3c9PSIsInZhbHVlIjoiTGJONnJy');
        $response = $client->getNext($request);

        $this->assertSame($request->currency, $response->getCurrency());
        $this->assertSame($request->orderId, $response->getOrderId());
    }

    public function testInvalid()
    {
        $request = new GetNextAddressRequest(
            '00e55aac-2e7d-11e8-a650-507b9dfbcb8f',
            md5((string) time()) . md5((string) time()),
            'BTC'
        );
        $client = new DepositAddressClient('http://deposit-addresses.paycoiner.loc', '1234567');

        $this->expectException(UnauthorizedRequest::class);
        $client->getNext($request);
    }
}
