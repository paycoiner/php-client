<?php

declare(strict_types=1);

namespace Tests\Connecting;

use Paycoiner\Client\Clients\PaymentClient;
use Paycoiner\Client\Models\Requests\CreateInvoiceRequest;
use Tests\TestCase;

class PaymentTestConnecting extends TestCase
{
    public function testCreate()
    {
        $client = new PaymentClient('http://payments.paycoiner.loc', 'eyJpdiI6Ik9nMU15clNEVWxlSHoxVWxESStGM3c9PSIsInZhbHVlIjoiTGJONnJy');
        $request = new CreateInvoiceRequest(
            '00e55aac-2e7d-11e8-a650-507b9dfbcb8f',
            time() . md5((string) time()),
            'BTC',
            'BTC',
            '0.00001',
            'buyer@mail.com'
        );
        $invoice = $client->createInvoice($request);
        $this->assertSame($request->orderId, $invoice->getOrderId());
        $this->assertSame($request->baseCurrency, $invoice->getBaseCurrency());
        $this->assertSame($request->amount, $invoice->getBaseAmount());
        $this->assertSame($request->amount, $invoice->getAmount());
        $this->assertSame($request->email, $invoice->getEmail());

        $invoice = $client->getInvoice($invoice->getInvoiceId());

        $this->assertSame($request->orderId, $invoice->getOrderId());
        $this->assertSame($request->baseCurrency, $invoice->getBaseCurrency());
        $this->assertSame($request->amount, $invoice->getBaseAmount());
        $this->assertSame($request->amount, $invoice->getAmount());
        $this->assertSame($request->email, $invoice->getEmail());
    }
}
